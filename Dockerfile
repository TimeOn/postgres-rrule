FROM postgres:9.6

RUN echo 'deb http://apt.postgresql.org/pub/repos/apt/ stretch-pgdg main' $PG_MAJOR > /etc/apt/sources.list.d/pgdg.list
RUN apt-get update
RUN apt-get install -y wget
RUN wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
RUN apt-get update
RUN apt-get install -y \
    postgresql-server-dev-$PG_MAJOR \
    pwgen \
    unzip \
    qt4-qmake \
    make \
    gcc \
    g++ \
    libical2 \
    libical-dev \
    libc6-dev \
		&& rm -rf /var/lib/apt/lists/*
	
RUN mkdir /tmp/pg_rrule \
 && cd /tmp/pg_rrule \
 && wget https://github.com/petropavel13/pg_rrule/archive/master.zip \
 && unzip master.zip \
 && cd pg_rrule-master/src \
 && ln -s /usr/include/postgresql/${PG_MAJOR}/server/ /usr/include/postgresql/server \
 && qmake-qt4 pg_rrule.pro \
 && make \
 && cp libpg_rrule.so /usr/lib/postgresql/${PG_MAJOR}/lib/pg_rrule.so \
 && cp ../pg_rrule.control /usr/share/postgresql/${PG_MAJOR}/extension \
 && cp ../sql/pg_rrule.sql.in /usr/share/postgresql/${PG_MAJOR}/extension/pg_rrule--0.2.0.sql
 
RUN apt-get purge -y --auto-remove \
  postgresql-server-dev-$PG_MAJOR \
  pwgen \
  unzip \
  qt4-qmake \
  make \
  gcc \
  g++ \
  libical-dev \
  libc6-dev \
  wget